/*
 * Copyright (c) 2014., Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.kenburns;

import javafx.animation.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Takes an image of a directory of images and displays them using <a href="https://en.wikipedia.org/wiki/Ken_Burns_effect">Ken Burns Effect</a>.
 *
 * @author markus@jevring.net
 */
public class Main extends Application {
	private final ImageView imageView = new ImageView();

	public Main() {
		imageView.setSmooth(true);
	}

	@Override
	public void start(Stage stage) throws Exception {
		final List<Path> files = new ArrayList<Path>();
		for (String s : getParameters().getUnnamed()) {
			Path path = Paths.get(s);
			Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					files.add(file);
					return super.visitFile(file, attrs);
				}
			});
		}
		
		StackPane stack = new StackPane();
		stack.getChildren().add(imageView);
		Scene scene = new Scene(stack);

		final ExecutorService executorService = Executors.newSingleThreadExecutor();
		// todo: also catch escape
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent windowEvent) {
				executorService.shutdownNow();
			}
		});
		
		stage.setTitle("Ken Burns Effect Viewer");
		stage.setWidth(1280);
		stage.setHeight(800);
		//stage.setFullScreen(true); // todo: later when we know it works
		stage.setScene(scene);
		stage.show();
		
		
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				showImages(files);
			}
		});
		
	}

	private void showImages(List<Path> files) {
		for (Path file : files) {
			try {
				System.out.println("Loading image: " + file);
				Image image = new Image(Files.newInputStream(file));
				Platform.runLater(() -> imageView.setImage(image));
				
				
				/*
				First zoom, then move, provided we don't leave any whitespace.
				The size of the move determines the size of the translation and the scale, so we don't end up outside
				 */
				
				
				// todo: reset the image view
				// todo: randomize how zoomed in the image is, and where it "starts"
				
				//imageView.setX(0);
				//imageView.setY(0);
				//imageView.setScaleX(0);
				//imageView.setScaleY(0);
				// it would be nice if we could get this to operate on imageView.getViewport() instead
				//Thread.sleep(2000);
				//imageView.setViewport(new Rectangle2D(100, 200, 300, 400));
				//imageView.resizeRelocate(0, 0, image.getWidth(), image.getHeight());
				/*
				ParallelTransitionBuilder.create().children(
						TranslateTransitionBuilder.create().toX(0).toY(0).build(),
				        ScaleTransitionBuilder.create().toX(1).toY(1).build()
				).build().play();
				*/
				//imageView.setScaleX(-1.5f);
				//imageView.setScaleY(-1.5f);
				System.out.println("Waiting to move");
				Thread.sleep(3000);

				Platform.runLater(() -> {
					// move the picture to some random location
					imageView.setTranslateX(image.getWidth() * (Math.random() / 4));
					imageView.setTranslateY(image.getHeight() * (Math.random() / 4));
					System.out.println("imageView.isResizable() = " + imageView.isResizable());
				});
				
				


				System.out.println("Waiting to resize");
				Thread.sleep(3000);
				boolean zoomOut = Math.random() > 0.5;
				double startResizeScale = 1 * Math.random();
				Platform.runLater(() -> {
					// todo: why doesn't the resize work?!?!?!
					if (zoomOut) {
						imageView.setFitWidth(image.getWidth() * startResizeScale);
						imageView.setFitHeight(image.getHeight() * startResizeScale);
						// todo: image needs to start zoomed in
					} else {
						// todo: image CAN start zoomed in, but doesn't have to
						imageView.setFitWidth(image.getWidth() * startResizeScale);
						imageView.setFitHeight(image.getHeight() * startResizeScale);
					}
				});
				

				

				System.out.println("Waiting to translate");
				Thread.sleep(3000);
				







				// todo: randomize the translation values within some range
				
				TranslateTransitionBuilder ttb = TranslateTransitionBuilder.create();
				ttb.byX(image.getWidth() * Math.random());
				ttb.byY(image.getHeight() * Math.random());
				ttb.duration(Duration.seconds(5));
				TranslateTransition translateTransition = ttb.build();

				ScaleTransitionBuilder stb = ScaleTransitionBuilder.create();
				
				// we have to use the scame scale on both axes, otherwise we'll fuck it up
				double scale;
				if (zoomOut) {
					scale = 1.5 * Math.random();
				} else {
					scale = -1.5 * Math.random();
				}
				stb.byX(scale).byY(scale);
				
				stb.duration(Duration.seconds(5));
				ScaleTransition scaleTransition = stb.build();
				

				ParallelTransitionBuilder ptb = ParallelTransitionBuilder.create();
				ptb.children(translateTransition, scaleTransition).node(imageView);
				ParallelTransition parallelTransition = ptb.build();
				
				parallelTransition.play();
				


				Thread.sleep(5000);
				
				
				
				// NOTE: We manage to create a new image instance even if the file isn't an image.
				// we'll have to deal with this elsewhere
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				return;
			}
		}

	}

	public static void main(String[] args) {
		launch(args);
	}
}
